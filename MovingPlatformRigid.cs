﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovingPlatformRigid : MonoBehaviour
{
    public Transform startPosition;
    public Transform endPosition;

    public float movementTheta = 1.5f;

    // Update is called once per frame
    private float movementTimer = 0;
    private bool pauseMovement = false;

    private FirstPersonController fpsController;

    private Rigidbody rgBody;
    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        rgBody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if(!pauseMovement) {
            rgBody.MovePosition(startPosition.position + (endPosition.position - startPosition.position) * ( (Mathf.Sin(movementTimer/movementTheta) + 1)/2));
            //transform.rotation = Quaternion.RotateTowards()) startPosition.rotation + (endPosition.rotation - startPosition.rotation) * ( (Mathf.Sin(movementTimer/movementTheta) + 1)/2);
             
            movementTimer += Time.fixedDeltaTime;
        }
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {
            fpsController = other.GetComponent<FirstPersonController>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player") {
            Debug.DrawRay(transform.position, rgBody.velocity, Color.cyan);
            fpsController.AddExtraMovement(rgBody.velocity * Time.deltaTime);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") {
            fpsController = null;
        }
    }
}
