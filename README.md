Unity First Person Controller
=======
Smaller script I've been working on for a couple of days. Goal is to create a fairly snappy and comfortable FPS-controller that you can use to quickly start working on more interesting game mechanics.

In general, I found most freely available scripts around the internet always ended up having some smaller issues or choices that I didnt particularly like, or they where just simply missing features I feel like are obvious base features. This script does contain the odd snippet here and there from tutorials and various scripts I found in other open sourced projects around the net, but I mostly coded this myself.

### Features so far
* Camera rotation and WASD object moving combined into the same script 
* Crouching and running *(you will need to define "Sprint" and "Crouch" in your input manager)*
    * You can only stop crouching if there's enough space above your head
    * Field of View can increase when you run.
* Camera smoothing
* A simple headbob
* You define jumping by describing the desidered jump height and arc time, instead of messing around with jump velocities and stuff. You can have a different arc for upwards and downwards momentum (slower up than down feels more snappy)
    * This is mainly based on the points presented by the following [GCD presentation](https://www.youtube.com/watch?v=hG9SzQxaCm8)
* Variable jump height depending on how long you press jump
* When the player is standing on an incline above the CharacterController slope limit, they slide down
* theres a function for adding external movement, which allows stuff like moving platforms (Plaforms are responsible of moving the character accordingly.) It still has some jittering when moving up and down (might be due to the antibump applied)
* Can (albeit kinda crudely) push on rigid body objects.

##### Features I wanna add (that I could think of right now atleast)
* Some basic logic for wall jumping and/or a double jump could be cool
* Ladders
* A version of air-control that slows you down, as opposed to just fully on/off
* Need to clean up the inspector UI, perhaps hide settings until they're relvant
    * If possible, it'd be real nice to draw the jump arc so you can get an idea of how it will look

##### Notable bugs / ugly things
* The capsule shape of the character controller causes some weird situations, this needs to be fixed:
    * You kinda squeeze partly into a hole meant for crouching 
    * You end kinda "hang" off ledges (the bottom of the collider is lower than the surface because you end up colliding halfway up the bottom sphere)


### Demo/test scene
I added demo scene to the repo, it's just a flat area with some cubes I've been using to test different interactions. ***I did not make the assets used here!!!*** The prefabs, materials, etc. used are from the [DigitalKonstrukt Prototyping Pack](https://assetstore.unity.com/packages/3d/prototyping-pack-free-94277). 