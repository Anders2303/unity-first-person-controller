﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour
{
    [Header("Camera")] //----------------------------------------------------------------------------------------------------------------------------
    // Input settings
    public float mouseSensitivity = 200;
    public bool clampHeadRotation = true;
    
    // Cam Smoothing
    [Range(0,25)] public float cameraSmoothing = 2;
    private Vector3 followAngles = Vector3.zero;
    private Vector3 currentCameraVelocity = Vector3.zero;
    private Vector3 targetAngles = Vector3.zero;
    
    // FOV
    public float normalFieldOfView = 90;
    public float runningFieldOfView = 100;
    public float fovChangeSpeed = 5;
    private float targetFov;

    // Headbob
    public bool enableHeadBob = true;
    public float headBobAmplitude = .05f;
    public float headBobPeriod = .07f;
    private float headbobMinimumMovement = 0.5f;


    [Space(6)] 
    [Header("Movement")] //--------------------------------------------------------------------------------------------------------------------------
    // Speed
    public float walkingSpeed = 6;
    public float crouchingSpeed = 4;
    public float runningSpeed = 12;
    public bool limitDiagonalSpeed = true;
    private float currentMovementSpeed;

    // Crouch / Sprint
    public bool toggleCrouch = false;
    public bool toggleSprint = false;
    public float crouchHeight = 1.2f;
    private bool isSprinting = false;
    private bool isCrouching = false;
    
    // Movement directions
    private Vector3 velocity;
    private Vector3 inputMoveDirection;
    
    [Space(6)]
    [Header("Jumping")] //---------------------------------------------------------------------------------------------------------------------------
    public bool allowAirMovement = false;
    
    // Jump arc definition
    public float jumpHeight = 3;
	public float timeToJumpApex = .6f;
	public float timeToFall = .4f;
	public float smalljumpVelocityMultiplier = .5f;
	
    // Derived jump forces
    private float jumpVelocity;
	private float gravity;
	private float fallGravity;
    
    // Tracker for registering the frame of impact
    private bool falling = false;
	
    // Input trackers
    private bool jump;
	private bool jumpCancel;

    [Space(6)]
    [Header("Ledges")] //----------------------------------------------------------------------------------------------------------------------------
    public bool slideWhenOverSlopeLimit = true;
    public bool ensureAntiBump = true;
    public float antiBumpFactor = .75f;
    public bool conserveMomentumIntoSlide = true;
    
    // TODO: I wanna have some settings to directly set how far off the edge you can "lean". Might just use a forced slide, or something
    //public bool avoidLedgeHanging = true;
    //public float footRadius = 0.05f;
   

    //-[ General derived variables] -----------------------------------------------------------------------------------------------------------------
    // Component refferences
    private Camera headCamera;
    private CharacterController charController;
    
    // Starting Values
    private float originalHeight;
    private Vector3 originalRotation;
    private Vector3 cameraLocalPosition;

    //-----------------------------------------------------------------------------------------------------------------------------------------------
    //--[ UNITY LIFETIME METHODS ]-------------------------------------------------------------------------------------------------------------------
    
    // when the object loads (in editor aswell)
    void Awake() {
        headCamera = GetComponentInChildren<Camera>();
        charController = GetComponent<CharacterController>();
        originalRotation = transform.localRotation.eulerAngles;
        originalHeight = charController.height;
        cameraLocalPosition = headCamera.transform.localPosition;

        jump = false;
		jumpCancel = false;

		gravity = -(2 * jumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		fallGravity = -(2 * jumpHeight) / Mathf.Pow (timeToFall, 2);
		jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;

    }
    
    // Right before the first frame
    void Start()
    {
        headCamera.fieldOfView = normalFieldOfView;

        // Lock cursor to screen
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }
    
    // Every frame
    void Update()
    {
        
        ReadMovementInput();
        UpdateCameraPosition();

        //charController.
        PerformMovement();
        addedExternalMovement = Vector3.zero;
    }

    
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    //--[ Private helper funtions ]------------------------------------------------------------------------------------------------------------------
    private void PerformMovement() {
        //velocity.y = 0;
        if(charController.isGrounded) {
            
            bool sliding = false;
            if (Vector3.Angle(mostRecentCollisionHit.normal, Vector3.up) > charController.slopeLimit) {
                sliding = true;
            }
            

            // Perform sliding on slope, or a slide tagged surface
            // if (mostRecentCollisionHit.collider.gameObject.tag == "Slide") {
            //    charController.slopeLimit = 180;
            //    PerformSlide();
            //}
            if (sliding && slideWhenOverSlopeLimit) {
                HandleSlide();
                ChangeCamFoV(headCamera.fieldOfView, runningFieldOfView);
            }
            else {
                if(inputMoveDirection.x == 0 && Mathf.Abs(velocity.x) > 0) {
                    velocity.x *= 0.8f;
                } else { velocity.x = inputMoveDirection.x * currentMovementSpeed; }                 
                
                if(inputMoveDirection.z == 0 && Mathf.Abs(velocity.z) > 0) {
                    velocity.z *= 0.8f;
                } else { velocity.z = inputMoveDirection.z * currentMovementSpeed; }

                velocity.y = -antiBumpFactor;
                if(ensureAntiBump) {
                    
                    // THIS IS HORRIBLE CODE, I couldnt think of a better way to "clamp" the velocity to not go above 90 degrees
                    int maxIterations = 20;
                    while (Vector3.Angle(-mostRecentCollisionHit.normal, velocity) > 90 || maxIterations < 0) {
                        velocity.y -= antiBumpFactor;
                        
                        maxIterations --;
                    }
                }

            }

            if (falling) {
                // This is where you'd want to perform any logic happening the frame the player lands
                falling = false;
            }

            if (jump) {
                velocity.y = 0;
			    velocity += mostRecentCollisionHit.normal * jumpVelocity + addedExternalMovement;//*jumpVelocity;//jumpVelocity;
			    jump = false;
		    }
            
        }
        else {
            Debug.Log(addedExternalMovement);

            // check for head bump
            if((charController.collisionFlags & CollisionFlags.Above) != 0 && velocity.y > 0) {
                velocity.y = 0;
            }
            
            // Do shortjump
            if (jumpCancel) {
                if (velocity.y > smalljumpVelocityMultiplier*jumpVelocity) {
                    velocity.y = smalljumpVelocityMultiplier*jumpVelocity;
                }
			    jumpCancel = false;
		    }

            if(allowAirMovement) {
                if(inputMoveDirection.x == 0 && Mathf.Abs(velocity.x) > 0) {
                    velocity.x *= 0.99f;
                } else { velocity.x = inputMoveDirection.x * currentMovementSpeed; }                 
                
                if(inputMoveDirection.z == 0 && Mathf.Abs(velocity.z) > 0) {
                    velocity.z *= 0.99f;
                } else { velocity.z = inputMoveDirection.z * currentMovementSpeed; }
            }
            
            // apply gravity
            if(velocity.y < 0) {
                velocity.y += fallGravity * Time.deltaTime;
            } 
            else {
                velocity.y += gravity * Time.deltaTime;
            }

            falling = true;
        }

        // Move the playerobject in the calculated direciton
        Debug.DrawRay(transform.position, velocity + addedExternalMovement, Color.green);
        charController.Move(addedExternalMovement);
        charController.Move(velocity * Time.deltaTime);
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        
    }
    
    
    private void ReadMovementInput() {  
        // JUMPIN'
        if (Input.GetButtonDown("Jump") && charController.isGrounded) {
			jump = true;
		}

		if (Input.GetButtonUp("Jump") && !charController.isGrounded) {
			jump = false;
			jumpCancel = true;
		}

        // WALKIN'
        float inputX = Input.GetAxis("Horizontal");
        float inputZ = Input.GetAxis("Vertical");
        
        inputMoveDirection = transform.right * inputX + transform.forward * inputZ;
        if (limitDiagonalSpeed && inputX != 0f && inputZ != 0f)
        {
            inputMoveDirection *= .7071f;
        }


        // Need to force the crouch if there is no space above to stand up
        if(isCrouching && Physics.Raycast(transform.position, Vector3.up, originalHeight - crouchHeight/2)) {
            isCrouching = true;
        }
        // TODO: This is sort of a mess of if/else stuff, clean it up a bit
        else if (toggleCrouch)
        {
            if (Input.GetButtonDown("Crouch")) {
                isCrouching = !isCrouching;
            }
        }
        else {
            isCrouching = Input.GetButton("Crouch");
        }

        // RUNNIN'
        if (toggleSprint)
        {
            if (Input.GetButtonDown("Sprint")) {
                isSprinting = !isSprinting;
            }
        }
        else {
            isSprinting = Input.GetButton("Sprint");
        }


        charController.height = isCrouching ? crouchHeight : originalHeight;
        targetFov = isSprinting && inputMoveDirection != Vector3.zero && !isCrouching ? runningFieldOfView : normalFieldOfView;
        ChangeCamFoV(headCamera.fieldOfView, targetFov);
        cameraLocalPosition.y = charController.height/2 - 0.2f;

        if(isCrouching)      { currentMovementSpeed = crouchingSpeed; }
        else if(isSprinting) { currentMovementSpeed = runningSpeed; }
        else                 { currentMovementSpeed = walkingSpeed; }
    }
    
    // Rotate the head over here
    private void UpdateCameraPosition() {
        // Camera movement
        targetAngles.x += Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        targetAngles.y -= Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        
        if(targetAngles.y > 180) {
            targetAngles.y -= 360;
            followAngles.y -= 360;
        } 
        else if(targetAngles.y < -180) { 
            targetAngles.y += 360; 
            followAngles.y += 360; 
        }
        
        if(targetAngles.x > 180) {
            targetAngles.x -= 360; followAngles.x -= 360;
        } 
        else if(targetAngles.x < -180) {
            targetAngles.x += 360;
            followAngles.x += 360; 
        }
        
        if (clampHeadRotation) { 
            targetAngles.y = Mathf.Clamp(targetAngles.y, -90f, 90f);
        }

        followAngles = Vector3.SmoothDamp(followAngles, targetAngles, ref currentCameraVelocity, (cameraSmoothing)/100);
        
        headCamera.transform.localRotation = Quaternion.Euler(followAngles.y + originalRotation.x,0,0);
        transform.localRotation = Quaternion.Euler(0, followAngles.x + originalRotation.y, 0);

        // Apply headbob
        if(enableHeadBob && charController.isGrounded && inputMoveDirection.magnitude > headbobMinimumMovement) {
            float theta = Time.timeSinceLevelLoad/headBobPeriod  * (currentMovementSpeed/walkingSpeed);
            float bobPosition = headBobAmplitude * Mathf.Sin(theta) * (charController.height/originalHeight);
            headCamera.transform.localPosition = cameraLocalPosition + Vector3.up * bobPosition;
        } else {
            headCamera.transform.localPosition = cameraLocalPosition;
        }
    }
    
    // Do bullshit math to make the user slide down whatever surface its currently on
    private void HandleSlide() {
        Vector3 hitNormal = mostRecentCollisionHit.normal;
        float gravityAngle = Vector3.Angle(Vector3.down, -hitNormal);
        
        Vector3 slideVelocity = new Vector3(hitNormal.x, -hitNormal.y, hitNormal.z);
        Vector3.OrthoNormalize(ref hitNormal, ref slideVelocity);
        
        // it gets weird on a flat slide with 0 user input
        if (gravityAngle == 0) {
            if(velocity.x == 0 && velocity.z == 0) {
                slideVelocity = Vector3.zero;
                slideVelocity.x = inputMoveDirection.x * currentMovementSpeed;
                slideVelocity.z = inputMoveDirection.z * currentMovementSpeed;
            } else {
                Vector3 temp = velocity;
                temp.y = 0;
                slideVelocity = Vector3.RotateTowards(slideVelocity, temp, 360, Mathf.Infinity);
            }
        }
        
        slideVelocity = slideVelocity.normalized * (velocity.magnitude - Mathf.Sin(gravityAngle) * fallGravity * Time.deltaTime);

        if(falling && conserveMomentumIntoSlide) {
            velocity = slideVelocity.normalized * velocity.magnitude;
        }
        
        // TODO: These lerps dont act nice when the player is falling straight down, or has speed going against the wall
        // We should avoid a "dead" stop if you're falling straight down
        velocity.x = Mathf.Lerp(velocity.x, slideVelocity.x, 2*Time.deltaTime);
        //velocity.x = slideVelocity.x;
        velocity.y = slideVelocity.y - antiBumpFactor;
        velocity.z = Mathf.Lerp(velocity.z, slideVelocity.z, 2*Time.deltaTime);
        //velocity.z = slideVelocity.z;
        /*
        else {
            velocity.y = slideVelocity.y - antiBumpFactor;
        }*/

        Debug.DrawRay(transform.position, slideVelocity, Color.magenta);
        

        // Could optionally ensure the player never reaches above a certain speed
        //velocity = Vector3.ClampMagnitude(velocity, maxSlideSpeed);
    }

    private Vector3 addedExternalMovement;

    // Smoothly changes the camera FoV over time perfferably use even without smoothing to stop any existing routine)
    private float previousGoal = -1;
    private void ChangeCamFoV(float currentFoV, float goalFoV, bool noSmoothing=false) {
        if (goalFoV == previousGoal) { return; }

        previousGoal = goalFoV;
        
        IEnumerator ChangeFovSmooth() {
            float lerpTime = 0; 
            
            while (lerpTime <= 1) {
                yield return new WaitForEndOfFrame();
                headCamera.fieldOfView = Mathf.Lerp(currentFoV, goalFoV, lerpTime);
                lerpTime += fovChangeSpeed * Time.deltaTime;
                
            }
            headCamera.fieldOfView = targetFov;
            yield return null;
        }

        StopCoroutine(ChangeFovSmooth());
        if (noSmoothing) {
            headCamera.fieldOfView = targetFov;
        } else {
            StartCoroutine(ChangeFovSmooth());
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------------------
    //--[ PUBLIC HELPER FUNTIONS ]-------------------------------------------------------------------------------------------------------
    
    // Simple function that teleports the controller to align with a given transform
    public void TeleportTo(Transform goal) {
        charController.enabled = false;
        transform.position = goal.position;
        targetAngles = Vector3.zero;
        followAngles = Vector3.zero;
        originalRotation = goal.rotation.eulerAngles;
        charController.enabled = true;
    }
   
    //-----------------------------------------------------------------------------------------------------------------------------------
    //--[ EVENTS ]-----------------------------------------------------------------------------------------------------------------------
    private ControllerColliderHit mostRecentCollisionHit;
    /// <summary>
    /// OnControllerColliderHit is called when the controller hits a
    /// collider while performing a Move.
    /// </summary>
    /// <param name="hit">The ControllerColliderHit data associated with this collision.</param>
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.rigidbody && hit.gameObject.tag != "MovingPlatform" ) {
            Debug.Log("aaa");
            Vector3 force = velocity;
            force.y = Mathf.Clamp(force.y, 0, Mathf.Infinity);
            hit.rigidbody.AddForceAtPosition(force, hit.point, ForceMode.Force);
        }
        
        mostRecentCollisionHit = hit;

        // Cancel velocity that points towards a wall you collided with
        if ((charController.collisionFlags & CollisionFlags.Sides) != 0) {
            velocity.x = charController.velocity.x;
            velocity.z = charController.velocity.z;
        }
    }

    public void DoMove(Vector3 movement) {
        charController.Move(movement);
    }

    public void AddExtraMovement(Vector3 movement) {
        addedExternalMovement += movement;
    }
}