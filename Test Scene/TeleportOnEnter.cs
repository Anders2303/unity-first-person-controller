﻿using UnityEngine;

public class TeleportOnEnter : MonoBehaviour
{
    public Transform teleportGoal;
    
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<FirstPersonController>().TeleportTo(teleportGoal);
        }
    }
}
