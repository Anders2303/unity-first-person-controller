﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform startPosition;
    public Transform endPosition;

    public float movementTheta = 1.5f;

    // Update is called once per frame
    private float movementTimer = 0;
    private bool pauseMovement = false;

    void Update()
    {
        if(!pauseMovement) {
            transform.position = startPosition.position + (endPosition.position - startPosition.position) * ( (Mathf.Sin(movementTimer/movementTheta) + 1)/2);
            //transform.rotation = Quaternion.RotateTowards()) startPosition.rotation + (endPosition.rotation - startPosition.rotation) * ( (Mathf.Sin(movementTimer/movementTheta) + 1)/2);
            
            movementTimer += Time.deltaTime;
        }
    }
}
